#include "MyForm.h"
//#include "AuthForm2.h"

using namespace System;
using namespace System::Windows::Forms;
using namespace Seti;

void Main(array<String^>^ args) {
    Application::EnableVisualStyles();
    Application::SetCompatibleTextRenderingDefault(false);
    Seti::MyForm form;
    Application::Run(% form);
}

System::Void Seti::MyForm::button1_Click(System::Object^ sender, System::EventArgs^ e)
{
    MyForm::Hide();
    AuthForm2^ form2 = gcnew AuthForm2();
    form2->ShowDialog();
}

System::Void Seti::MyForm::button2_Click(System::Object^ sender, System::EventArgs^ e)
{
    Application::Exit();
}
